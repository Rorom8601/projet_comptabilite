import { Operation } from './operation';
import { Category } from './operation';
import { Compte } from './Compte';
import './style.css'

//recupération des elements HTML
const solde = document.querySelector("#solde");
const btnValidate = document.querySelector<HTMLElement>('#btn-validate');
const liste = document.querySelector<HTMLInputElement>("#list");
const montant = document.querySelector<HTMLInputElement>("#Montant");
const categorie = document.querySelector<HTMLInputElement>("#Categorie");
const libelle = document.querySelector<HTMLInputElement>("#Libelle");
const date = document.querySelector<HTMLInputElement>("#date");
const btnCancel = document.querySelector<HTMLButtonElement>("#btn-cancel");
var radioBtnType = document.getElementsByName('type-expense');
console.log(radioBtnType);

//Création du compte
let compte: Compte = new Compte([], 0);

//création d'une liste d'opération
let operation1: Operation = new Operation(50, Category[0], "resto", new Date(2022, 4, 4,), "expense");
let operation2: Operation = new Operation(30, Category[6].toString(), "la chance", new Date(2021, 10, 3), "income");
let operation3: Operation = new Operation(1300, Category[4], "Avril", new Date(2022, 5, 1), "income");
let operation4: Operation = new Operation(50, Category[2], "besoin cash", new Date(2022, 2, 2), "expense");
compte.addOperation(operation1);
compte.addOperation(operation2);
compte.addOperation(operation3);
compte.addOperation(operation4);
//Affichage de la liste au premier chargement de la page
printList(compte.listOperation);
//affichage du solde au chargement de la page
printTotalAmout();
//mise en place des categories de l'enum dans liste déroulante
for (const cat of Object.keys(Category)) {

  if (isNaN(Number(cat))) {
    //console.log(cat);
    const option = document.createElement('option');
    option.value = cat;
    option.text = cat;
    categorie?.append(option);
  }

}


//Sur le click de Valider:
btnValidate?.addEventListener('click', () => {
  //check if montant is a number
  if (isNaN(Number(montant?.value))) {
    montant?.classList.add('inputIncorrect');
  }
  else {
    montant?.classList.remove('inputIncorrect');
    //Get value of radio button
    const checked = document.querySelector('input[name=type-expense]:checked');
    let type = checked!.id;
    let mtn: number = Number(montant!.value);
    let cat: string = categorie!.value;
    let lib: string = libelle!.value;
    let dateOperation = date?.valueAsDate;

    if (dateOperation) {
      let ope: Operation = new Operation(mtn, cat, lib, dateOperation, type);
      //Insertion dans la liste d'opérations
      compte.addOperation(ope);
      //Raffraichissement de la liste, remplissage liste + nouveau solde
      clearList();
      printList(compte.listOperation);
      printTotalAmout();
      clearFields();
      // console.log(compte.solde);
      // console.log(type);

    }
  }

});
btnCancel?.addEventListener('click', () => {
  clearFields();
});

//functions
function printList(listOfOperation: Operation[]) {
  listOfOperation.forEach(operation => {
    let dd = String(operation.date.getDate()).padStart(2, '0');
    let mm = String(operation.date.getMonth() + 1).padStart(2, '0');
    let year = operation.date.getFullYear();
    let dateToPrint = dd + "/" + mm + "/" + year;
    let newPara = document.createElement("p");
    if (operation.type == "income") {
      newPara.classList.add('green');
    }
    newPara.classList.add('red');
    let content = operation.label + "-" + operation.category + "-"
      + operation.amount + " euros - " + dateToPrint;
    newPara.innerHTML = content + "<button id='btn-right'>Supprimer</button>";
    const btnErase = newPara.querySelector("#btn-right");
    liste?.appendChild(newPara);
    btnErase?.addEventListener('click', () => {
      newPara.remove();
      compte.deleteOperation(operation);
      printTotalAmout();
    })
  })

}
function printTotalAmout() {
  solde!.textContent = compte.solde.toString() + " euros";
  if ((compte.solde) < 0) {
    solde!.classList.add('red');
    solde!.classList.remove('green');
  }
  else {
    solde!.classList.add('green');
    solde!.classList.remove('red');
  }

}

function clearList() {
  liste!.innerHTML = '';
}


function clearFields() {
  montant!.value = '';
  libelle!.value = '';
  categorie!.value = '';
  date!.value = '';
}








