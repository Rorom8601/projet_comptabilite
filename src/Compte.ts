import { Operation } from "./operation";

export class Compte {
  listOperation: Operation[];
  solde: number;

  constructor(listOperation: Operation[], solde: number) {
    this.listOperation = listOperation,
      this.solde = solde
  }
  addOperation(operation: Operation) {
    this.listOperation.push(operation);
    if (operation.type == 'expense') {
      operation.amount = -(Math.abs(operation.amount));
    }
    this.solde += operation.amount;
  }
  deleteOperation(operation: Operation) {
    this.listOperation = this.listOperation.filter((item) => item != operation);
    this.solde -= operation.amount;
  }
}