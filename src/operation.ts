export class Operation {

  amount: number;
  category: string;
  label: string;
  type: string;
  date: Date;




  constructor(amount: number, category: string, label: string, date: Date, type: string) {
    this.amount = amount,
      this.category = category,
      this.label = label,
      this.date = date,
      this.type = type

  }
}

//enum caterory
export enum Category {
  "Nourriture",
  "Transport",
  "Retrait",
  "Divers",
  "Salaire",
  "Gain au loto",
  "Gain au poker"
}
